FROM adoptopenjdk:11-jre-hotspot

ADD build/libs/twinsingle-bot-*.jar /twinsingle-bot.jar

CMD ["java", "-Duser.timezone=UTC", "-jar", "/twinsingle-bot.jar", "--spring.profiles.active=prod"]
