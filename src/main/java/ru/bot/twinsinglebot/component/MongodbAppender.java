package ru.bot.twinsinglebot.component;

import ch.qos.logback.classic.Logger;
import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.classic.spi.IThrowableProxy;
import ch.qos.logback.classic.spi.LoggingEvent;
import ch.qos.logback.classic.spi.StackTraceElementProxy;
import ch.qos.logback.core.UnsynchronizedAppenderBase;
import lombok.RequiredArgsConstructor;
import one.util.streamex.StreamEx;
import org.slf4j.LoggerFactory;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import ru.bot.twinsinglebot.data.document.LoggingEventMongo;
import ru.bot.twinsinglebot.data.document.ThrowableProxyMongo;
import ru.bot.twinsinglebot.data.repository.mongo.LoggingEventMongoRepository;

import javax.annotation.PostConstruct;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

import static java.time.LocalDateTime.ofEpochSecond;

@Component
@RequiredArgsConstructor
public class MongodbAppender extends UnsynchronizedAppenderBase<ILoggingEvent> implements ApplicationRunner {

    private static final Logger ROOT_LOGGER = (Logger) LoggerFactory.getLogger("ROOT");

    private static final DateTimeFormatter dtf = DateTimeFormatter.ISO_DATE_TIME;

    private final LoggingEventMongoRepository repository;

    @PostConstruct
    private void init() {
        setName("MongodbAppender");
        setContext(ROOT_LOGGER.getLoggerContext());
        ROOT_LOGGER.addAppender(this);
    }

    @Override
    protected void append(ILoggingEvent eventObject) {
        var event = (LoggingEvent) eventObject;
        var eventMongo = new LoggingEventMongo();

        eventMongo.setLevel(event.getLevel().levelStr);
        eventMongo.setTimeStamp(event.getTimeStamp());

        var seconds = event.getTimeStamp() / 1000;
        var nanoOfSecond = (int) (event.getTimeStamp() % 1000) * 1000000;
        var date = ofEpochSecond(seconds, nanoOfSecond, ZoneOffset.UTC).format(dtf);
        eventMongo.setDate(date);

        eventMongo.setMessage(event.getFormattedMessage());
        eventMongo.setThreadName(event.getThreadName());
        eventMongo.setLoggerName(event.getLoggerName());

        var throwableProxy = event.getThrowableProxy();
        if (throwableProxy != null) {
            eventMongo.setProxy(parse(throwableProxy));
        }

        repository.save(eventMongo);
    }

    private ThrowableProxyMongo parse(IThrowableProxy throwableProxy) {
        var proxy = new ThrowableProxyMongo();
        proxy.setClassName(throwableProxy.getClassName());
        proxy.setMessage(throwableProxy.getMessage());
        proxy.setSte(StreamEx
                .of(throwableProxy.getStackTraceElementProxyArray())
                .map(StackTraceElementProxy::getSTEAsString)
                .toList());

        if (throwableProxy.getCause() != null) {
            proxy.setCause(parse(throwableProxy.getCause()));
        }

        return proxy;
    }

    @Override
    public void run(ApplicationArguments args) {
        start();
    }

}
