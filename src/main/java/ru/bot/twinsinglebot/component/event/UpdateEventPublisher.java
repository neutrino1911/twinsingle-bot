package ru.bot.twinsinglebot.component.event;

import lombok.AllArgsConstructor;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.objects.Update;
import ru.bot.twinsinglebot.component.TelegramBot;

@Component
@AllArgsConstructor
public class UpdateEventPublisher {

    private final ApplicationEventPublisher applicationEventPublisher;

    public void publishEvent(Update update, TelegramBot bot) {
        applicationEventPublisher.publishEvent(new UpdateEvent(this, update, bot));
    }

}
