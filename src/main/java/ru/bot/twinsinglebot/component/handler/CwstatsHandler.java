package ru.bot.twinsinglebot.component.handler;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.bot.twinsinglebot.component.TelegramBot;
import ru.bot.twinsinglebot.data.pojo.cw.ChipWarResult;
import ru.bot.twinsinglebot.data.repository.mongo.ChipWarResultRepository;
import ru.bot.twinsinglebot.data.repository.mongo.UserRepository;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;

import static java.lang.String.format;
import static java.time.LocalDateTime.ofEpochSecond;
import static java.time.ZoneOffset.ofHours;
import static java.time.temporal.ChronoUnit.HOURS;
import static org.apache.commons.lang3.StringUtils.equalsAny;

@Slf4j
//@Component
@RequiredArgsConstructor
public class CwstatsHandler implements CommandHandler {

    private static final String COMMAND = "/cwstats";

    private final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm dd/MM/YYYY");

    private final UserRepository userRepository;

    private final ChipWarResultRepository chipWarResultRepository;

    @Override
    public void handle(Update update, TelegramBot bot) throws TelegramApiException {
        var message = update.getMessage();
        var sendMessage = new SendMessage();
        sendMessage.setChatId(message.getChat().getId());
        sendMessage.setReplyToMessageId(message.getMessageId());

        var userOptional = userRepository.findById(message.getFrom().getId());
        if (userOptional.isEmpty()) {
            sendMessage.setText("Профиль не найден. Перешли в лс профиль героя и попробуй еще раз.");
        } else {
            var user = userOptional.get();
            if (!equalsAny(user.getGuild(), "[TwinSingle]", "[TwinS]")
                    && !equalsAny(user.getRfId(), "5cda82c27929212061", "53dd84c572212c2265")) {
                sendMessage.setText("Эта фича только для участников гильдии TwinSingle!");
            } else {
                var split = message.getText().split(" ");
                ChipWarResult result = null;
                if (split.length > 1) {
                    if (!equalsAny(split[1], "10", "15", "20")) {
                        sendMessage.setText("Укажите время проведения ЧВ (10, 15, 20)");
                    } else {
                        var zoneOffset = ZoneOffset.ofHours(3);
                        var ldt = LocalDateTime.now(zoneOffset).truncatedTo(HOURS);
                        var date = (int) ldt.withHour(Integer.parseInt(split[1])).toEpochSecond(zoneOffset);
                        result = chipWarResultRepository.findByDate(date);
                    }
                } else {
                    result = chipWarResultRepository.findFirstByOrderByDateDesc();
                }
                if (result != null) {
                    sendMessage.setText(buildMessage(result));
                } else {
                    sendMessage.setText("Статистика появится после следующей ЧВ");
                }
            }
        }

        bot.execute(sendMessage);
    }

    private String buildMessage(ChipWarResult result) {
        var builder = new StringBuilder();
        builder.append(ofEpochSecond(result.getDate(), 0, ofHours(3)).format(dtf));
        for (var userResult : result.getResults()) {
            builder.append(format("\n%s %d/%d",
                    userResult.getNick(),
                    userResult.getWins(),
                    userResult.getLoses()
            ));
        }
        return builder.toString();
    }

    @Override
    public String getCommand() {
        return COMMAND;
    }

}
