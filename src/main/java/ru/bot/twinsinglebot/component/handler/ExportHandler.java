package ru.bot.twinsinglebot.component.handler;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVPrinter;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendDocument;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;
import ru.bot.twinsinglebot.component.TelegramBot;
import ru.bot.twinsinglebot.data.pojo.User;
import ru.bot.twinsinglebot.data.repository.mongo.UserRepository;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import static java.time.LocalDateTime.ofEpochSecond;
import static java.time.ZoneOffset.ofHours;
import static java.time.format.DateTimeFormatter.ISO_DATE_TIME;
import static org.apache.commons.lang3.StringUtils.equalsAny;

@Slf4j
@Component
@RequiredArgsConstructor
public class ExportHandler implements CommandHandler {

    private static final String COMMAND = "/export";

    private final UserRepository userRepository;

    private final ObjectMapper objectMapper;

    @Override
    public void handle(Update update, TelegramBot bot) throws TelegramApiException {
        var message = update.getMessage();
        if (message.getChatId() == -1001194710746L || message.getFrom().getId() == 49840383) {
            var split = message.getText().split(" ");
            var users = StreamEx
                    .of(userRepository.findAll())
                    .filter(e -> split.length == 1 || equalsAny(e.getGuild(), split))
                    .sorted(Comparator.comparingInt(User::getLevel).reversed())
                    .toList();

            var bytes = createCsv(users);
            var in = new ByteArrayInputStream(bytes);

            var sendDocument = new SendDocument();
            sendDocument.setChatId(message.getChat().getId());
            sendDocument.setReplyToMessageId(message.getMessageId());
            sendDocument.setDocument("export.csv", in);
            try {
                bot.execute(sendDocument);
            } catch (TelegramApiRequestException e) {
                log.error(e.toString(), e);
            }
        }
    }

    @Override
    public String getCommand() {
        return COMMAND;
    }

    @SneakyThrows(IOException.class)
    private byte[] createCsv(List<User> users) {
        var builder = new StringBuilder();
        var csvPrinter = new CSVPrinter(builder, CSVFormat.DEFAULT);
        var first = true;
        for (var user : users) {
            var map = objectMapper.convertValue(user, Map.class);
            map.computeIfPresent("profileDate", this::formatDate);
            map.computeIfPresent("lastUpdate", this::formatDate);
            if (first) {
                csvPrinter.printRecord(map.keySet());
                first = false;
            }
            csvPrinter.printRecord(map.values());
        }
        return builder.toString().getBytes();
    }

    private Object formatDate(Object key, Object obj) {
        var date = (Integer) obj;
        if (date == 0) {
            return "";
        } else {
            return ofEpochSecond(date, 0, ofHours(3)).format(ISO_DATE_TIME);
        }

    }

}
