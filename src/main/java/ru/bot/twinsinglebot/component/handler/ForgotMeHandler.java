package ru.bot.twinsinglebot.component.handler;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.bot.twinsinglebot.component.TelegramBot;
import ru.bot.twinsinglebot.data.repository.mongo.UserRepository;

@Slf4j
@Component
@RequiredArgsConstructor
public class ForgotMeHandler implements CommandHandler {

    private static final String COMMAND = "/forgot_me";

    private final UserRepository userRepository;

    @Override
    public void handle(Update update, TelegramBot bot) throws TelegramApiException {
        var message = update.getMessage();
        var sendMessage = new SendMessage();
        sendMessage.setChatId(message.getChat().getId());
        sendMessage.setReplyToMessageId(message.getMessageId());

        var userById = userRepository.findById(message.getFrom().getId());

        userById.ifPresentOrElse(entity -> {
            userRepository.delete(entity);
            sendMessage.setText("И запомни, мы с тобой никогда не виделись \uD83D\uDE0E");
        }, () -> sendMessage.setText("Я тебя и так знать не знаю"));

        bot.execute(sendMessage);
    }

    @Override
    public String getCommand() {
        return COMMAND;
    }

}
