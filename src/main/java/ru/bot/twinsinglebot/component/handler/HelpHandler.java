package ru.bot.twinsinglebot.component.handler;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.bot.twinsinglebot.component.TelegramBot;

@Slf4j
@Component
@RequiredArgsConstructor
public class HelpHandler implements CommandHandler {

    private static final String COMMAND = "/help";

    @Override
    public void handle(Update update, TelegramBot bot) throws TelegramApiException {
        var message = update.getMessage();
        var sendMessage = new SendMessage();
        sendMessage.setChatId(message.getChat().getId());
        sendMessage.setReplyToMessageId(message.getMessageId());
        sendMessage.setText("https://telegra.ph/FAQ-po-ispolzovaniyu-twinsingle-bot-06-18");
        bot.execute(sendMessage);
    }

    @Override
    public String getCommand() {
        return COMMAND;
    }

}
