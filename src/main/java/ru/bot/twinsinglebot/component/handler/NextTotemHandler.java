package ru.bot.twinsinglebot.component.handler;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import ru.bot.twinsinglebot.component.TelegramBot;
import ru.bot.twinsinglebot.data.pojo.TotemResult;
import ru.bot.twinsinglebot.data.repository.mongo.UserRepository;
import ru.bot.twinsinglebot.service.TotemService;

import java.util.List;

import static java.lang.String.format;

@Slf4j
@Component
@RequiredArgsConstructor
public class NextTotemHandler implements CommandHandler {

    private static final String COMMAND = "/nexttotem";

    private final UserRepository userRepository;

    private final TotemService totemService;

    @Override
    public void handle(Update update, TelegramBot bot) throws TelegramApiException {
        var message = update.getMessage();
        var sendMessage = new SendMessage();
        sendMessage.setChatId(message.getChat().getId());
        sendMessage.setReplyToMessageId(message.getMessageId());

        var userOptional = userRepository.findById(message.getFrom().getId());
        if (userOptional.isEmpty()) {
            sendMessage.setText("Профиль не найден. Перешли в лс профиль героя и попробуй еще раз.");
        } else {
            var user = userOptional.get();
            if (!"Basilaris".equals(user.getRace())) {
                sendMessage.setText("Эта фича только для расы \uD83D\uDC69\u200D\uD83D\uDE80Basilaris!");
            } else {
                var totemResults = totemService.nextTotems(user, 1);
                sendMessage.setText(buildMessage(totemResults));
                sendMessage.enableMarkdown(true);
            }
        }

        bot.execute(sendMessage);
    }

    private String buildMessage(List<TotemResult> results) {
        var builder = new StringBuilder();
        builder.append("```\n");
        for (var result : results) {
            builder.append(format("%s lvl:%d cost:%d%n",
                    result.getTotem().godName(),
                    result.getLevel(),
                    result.getCost()
            ));
        }
        builder.append("```");
        return builder.toString();
    }

    @Override
    public String getCommand() {
        return COMMAND;
    }

}
