package ru.bot.twinsinglebot.config;

import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;
import org.telegram.telegrambots.bots.DefaultBotOptions;

import javax.validation.constraints.NotEmpty;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

@Data
@NoArgsConstructor
@Validated
@Component
@ConfigurationProperties("telegram.bot")
public class TelegramBotProperties {

    @NotEmpty
    private String token;

    @NotEmpty
    private String name;

    @NotEmpty
    private String identifier;

    private boolean silence = true;

    private int botId;

    private long chatId;

    private long mainChatId;

    private Map<Long, Set<Integer>> admins = new ConcurrentHashMap<>();

    private Proxy proxy;

    @Data
    @NoArgsConstructor
    @Validated
    public static class Proxy {

        private DefaultBotOptions.ProxyType type;

        private String host;

        private int port;

    }

}
