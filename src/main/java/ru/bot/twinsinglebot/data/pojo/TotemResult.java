package ru.bot.twinsinglebot.data.pojo;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import ru.bot.twinsinglebot.data.enums.Totem;

import java.math.BigDecimal;

@Getter
@Setter
@EqualsAndHashCode
@ToString
@AllArgsConstructor
public class TotemResult {

    private Totem totem;

    private int level;

    private int cost;

    private int wins;

    private BigDecimal adenPerWin;

}
