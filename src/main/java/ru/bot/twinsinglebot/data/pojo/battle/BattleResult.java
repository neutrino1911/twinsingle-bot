package ru.bot.twinsinglebot.data.pojo.battle;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

import java.math.BigDecimal;
import java.math.RoundingMode;

import static java.math.BigDecimal.valueOf;

@Getter
@EqualsAndHashCode
@ToString
public class BattleResult {

    private Unit unit;

    private int wins;

    private int loses;

    private BigDecimal winRate;

    private BigDecimal expByWinRate;

    public BattleResult(Unit unit, int wins, int loses) {
        this.unit = unit;
        this.wins = wins;
        this.loses = loses;
        this.winRate = valueOf(wins).divide(valueOf((long) wins + loses), 10, RoundingMode.DOWN);
        this.expByWinRate = winRate.multiply(valueOf(unit.getExp()));
    }

}
