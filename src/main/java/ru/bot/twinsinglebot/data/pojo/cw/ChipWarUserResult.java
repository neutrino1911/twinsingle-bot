package ru.bot.twinsinglebot.data.pojo.cw;

import lombok.Data;

@Data
public class ChipWarUserResult {

    private String nick;

    private int userId;

    private int wins;

    private int loses;

}
