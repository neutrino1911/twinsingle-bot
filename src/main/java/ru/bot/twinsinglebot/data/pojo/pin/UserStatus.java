package ru.bot.twinsinglebot.data.pojo.pin;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.Accessors;

@Getter
@AllArgsConstructor
@Accessors(fluent = true)
public enum UserStatus {

    ON_THE_WAY("\uD83C\uDFC3 Иду", "\uD83C\uDFC3"),
    DEAD("☠ ️Умер", "☠"),
    READY("\uD83D\uDC4A На месте", "\uD83D\uDC4A");

    private String value;

    private String icon;

}
