package ru.bot.twinsinglebot.data.pojo.pin.castle;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import ru.bot.twinsinglebot.data.pojo.pin.AbstractUserStatus;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class CastleUserStatus extends AbstractUserStatus {

    private int kills;

    private int deaths;

}
