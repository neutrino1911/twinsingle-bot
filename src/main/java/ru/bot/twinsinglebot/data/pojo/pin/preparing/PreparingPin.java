package ru.bot.twinsinglebot.data.pojo.pin.preparing;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import ru.bot.twinsinglebot.data.pojo.pin.AbstractPin;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class PreparingPin extends AbstractPin<PreparingUserStatus> {

    private InlineKeyboardMarkup keyboard;

}
