package ru.bot.twinsinglebot.data.pojo.pin.preparing;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;
import ru.bot.twinsinglebot.data.pojo.pin.AbstractUserStatus;

@Data
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
public class PreparingUserStatus extends AbstractUserStatus {

    private String selection;

    private long debounce;

}
