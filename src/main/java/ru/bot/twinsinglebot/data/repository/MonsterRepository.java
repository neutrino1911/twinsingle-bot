package ru.bot.twinsinglebot.data.repository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;
import ru.bot.twinsinglebot.data.pojo.battle.Monster;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.toMap;

@Slf4j
@Repository
@RequiredArgsConstructor
public class MonsterRepository {

    private Map<Integer, Monster> monsterMap;

    private List<Monster> monsterList;

    {
        var list = new ArrayList<Monster>();

        list.add(new Monster(2, 31, 36, 4, 4, 20, 15, 20));
        list.add(new Monster(3, 55, 40, 4, 4, 15, 25, 25));
        list.add(new Monster(4, 77, 42, 7, 4, 20, 35, 30));
        list.add(new Monster(5, 110, 45, 6, 4, 15, 45, 38));
        list.add(new Monster(6, 149, 49, 8, 4, 15, 55, 35));
        list.add(new Monster(7, 182, 52, 10, 4, 20, 60, 43));
        list.add(new Monster(8, 255, 68, 11, 4, 15, 65, 45));
        list.add(new Monster(9, 280, 74, 14, 4, 20, 75, 50));
        list.add(new Monster(10, 350, 95, 18, 4, 20, 85, 70));
        list.add(new Monster(11, 370, 100, 17, 4, 15, 95, 70));
        list.add(new Monster(12, 400, 107, 18, 4, 15, 100, 70));
        list.add(new Monster(13, 420, 115, 20, 4, 15, 115, 75));
        list.add(new Monster(14, 440, 123, 25, 4, 15, 130, 85));
        list.add(new Monster(15, 460, 135, 27, 4, 15, 145, 95));
        list.add(new Monster(16, 500, 150, 28, 4, 15, 160, 100));
        list.add(new Monster(17, 520, 155, 28, 4, 15, 170, 115));
        list.add(new Monster(18, 550, 165, 28, 4, 15, 200, 120));
        list.add(new Monster(19, 650, 195, 30, 4, 15, 210, 125));
        list.add(new Monster(20, 680, 220, 31, 4, 15, 220, 135));
        list.add(new Monster(21, 700, 250, 34, 4, 11, 260, 165));
        list.add(new Monster(22, 710, 260, 37, 4, 11, 300, 205));
        list.add(new Monster(23, 720, 270, 40, 4, 11, 330, 220));
        list.add(new Monster(24, 780, 285, 40, 4, 11, 360, 225));
        list.add(new Monster(25, 800, 295, 41, 4, 11, 370, 225));
        list.add(new Monster(26, 820, 315, 42, 4, 11, 385, 225));
        list.add(new Monster(27, 860, 330, 44, 4, 11, 395, 225));
        list.add(new Monster(28, 890, 350, 45, 4, 11, 400, 225));
        list.add(new Monster(29, 920, 380, 46, 4, 11, 410, 230));
        list.add(new Monster(30, 990, 400, 55, 4, 11, 500, 300));
        list.add(new Monster(31, 1100, 450, 55, 4, 15, 500, 400));
        list.add(new Monster(32, 1250, 460, 60, 4, 15, 515, 400));
        list.add(new Monster(33, 1300, 470, 63, 4, 15, 530, 450));
        list.add(new Monster(34, 1400, 480, 70, 4, 15, 560, 450));
        list.add(new Monster(35, 1430, 490, 75, 4, 15, 590, 500));
        list.add(new Monster(36, 1470, 500, 78, 4, 15, 610, 500));
        list.add(new Monster(37, 1550, 510, 78, 4, 15, 630, 500));
        list.add(new Monster(38, 1570, 570, 80, 4, 15, 650, 500));
        list.add(new Monster(39, 1600, 600, 80, 4, 15, 670, 500));
        list.add(new Monster(40, 1650, 620, 85, 4, 15, 700, 500));
        list.add(new Monster(41, 1750, 650, 87, 4, 15, 710, 510));
        list.add(new Monster(42, 1850, 700, 90, 4, 15, 730, 520));
        list.add(new Monster(43, 1900, 720, 93, 4, 15, 750, 530));
        list.add(new Monster(44, 1950, 830, 97, 4, 15, 760, 540));
        list.add(new Monster(45, 1970, 890, 102, 4, 15, 770, 560));
        list.add(new Monster(46, 2000, 960, 105, 4, 15, 780, 570));
        list.add(new Monster(47, 2050, 1100, 110, 4, 15, 800, 580));
        list.add(new Monster(48, 2100, 1250, 113, 4, 15, 810, 590));
        list.add(new Monster(49, 2150, 1400, 117, 4, 15, 820, 600));
        list.add(new Monster(50, 2200, 1600, 125, 4, 15, 830, 610));

        monsterMap = Map.copyOf(list.stream().collect(toMap(Monster::getLevel, e -> e)));
        monsterList = List.copyOf(list);
    }

    public List<Monster> getAll() {
        return monsterList;
    }

    public Monster getByLevel(int level) {
        return monsterMap.get(level);
    }

}

