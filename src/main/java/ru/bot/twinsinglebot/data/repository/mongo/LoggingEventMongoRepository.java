package ru.bot.twinsinglebot.data.repository.mongo;

import org.springframework.data.mongodb.repository.MongoRepository;
import ru.bot.twinsinglebot.data.document.LoggingEventMongo;

public interface LoggingEventMongoRepository extends MongoRepository<LoggingEventMongo, String> {

}
