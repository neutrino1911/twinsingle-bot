package ru.bot.twinsinglebot.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.bot.twinsinglebot.data.pojo.battle.BattleResult;
import ru.bot.twinsinglebot.data.pojo.battle.Monster;
import ru.bot.twinsinglebot.data.pojo.battle.Unit;
import ru.bot.twinsinglebot.data.repository.MonsterRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.ConcurrentHashMap;

import static java.lang.Math.ceil;
import static java.lang.Math.max;
import static java.lang.String.format;
import static java.util.Comparator.comparing;
import static java.util.Comparator.comparingInt;

@Slf4j
@Service
@RequiredArgsConstructor
public class BattleService {

    private static final int SAMPLES = 100_000;

    private final Map<String, List<BattleResult>> topByExpCache = new ConcurrentHashMap<>();

    private final Map<String, BattleResult> computeResultCache = new ConcurrentHashMap<>();

    private final MonsterRepository monsterRepository;

    public List<BattleResult> topByExp(Unit user, int count) {
        var key = obtainKey(user, count);
        log.debug("topByExp key: {}", key);
        var results = topByExpCache.get(key);
        if (results != null) {
            log.debug("topByExp return from topByExpCache");
            return results;
        }

        results = new ArrayList<>();
        var level = user.getLevel();
        var maxLevel = level < 3 ? level + 2 : ((level - 1) / 10 + 1) * 10;
        log.debug("topByExp level: {}; maxLevel: {}", level, maxLevel);

        var all = new ArrayList<>(monsterRepository.getAll());
        all.removeIf(e -> e.getLevel() > maxLevel);
        all.sort(comparingInt(Monster::getExp).reversed());

        for (var monster : all) {
            if (results.size() >= count && results.get(count - 1).getExpByWinRate().intValue() >= monster.getExp()) {
                break;
            }
            results.add(computeResult(user, monster));
            results.sort(comparing(BattleResult::getExpByWinRate).reversed());
        }

        results = results.size() > count ? results.subList(0, count) : results;
        if (!results.isEmpty()) {
            topByExpCache.put(key, results);
        }

        log.debug("topByExp results: {}", results);
        return results;
    }

    private String obtainKey(Unit user, int count) {
        return format("%.2f:%.2f:%.2f:%.2f:%.2f:%d",
                user.getHealth(),
                user.getDamage(),
                user.getDefense(),
                user.getDodge(),
                user.getPrecision(),
                count
        );
    }

    public BattleResult computeResult(Unit user, int monsterLevel) {
        var key = obtainKey(user, monsterLevel);
        log.debug("computeResult key: {}", key);
        var result = computeResultCache.get(key);
        if (result != null) {
            log.debug("computeResult return from computeResultCache");
            return result;
        }

        result = computeResult(user, monsterRepository.getByLevel(monsterLevel));

        computeResultCache.put(key, result);

        log.debug("computeResult result: {}", result);
        return result;
    }

    BattleResult computeResult(Unit hero, Unit enemy) {
        var rnd = new Random(1181783497276652981L);
        int wins = 0;
        int loses = 0;
        for (int i = 0; i < SAMPLES; i++) {
            if (compute(rnd, hero, enemy, i % 2 == 0)) {
                wins++;
            } else {
                loses++;
            }
        }

        return new BattleResult(enemy, wins, loses);
    }

    private boolean compute(Random rnd, Unit hero, Unit enemy, boolean heroTurn) {
        double heroHealth = hero.getHealth();
        double enemyHealth = enemy.getHealth();

        while (heroHealth > 0 && enemyHealth > 0) {
            if (heroTurn) {
                enemyHealth -= turn(
                        rnd,
                        hero.getDamage(),
                        enemy.getDefense(),
                        enemy.getDodge(),
                        hero.getPrecision()
                );
            } else {
                heroHealth -= turn(
                        rnd,
                        enemy.getDamage(),
                        hero.getDefense(),
                        hero.getDodge(),
                        enemy.getPrecision()
                );
            }
            heroTurn = !heroTurn;
        }

        return heroHealth > 0;
    }

    private double turn(Random rnd, double damage, double defense, double dodge, double precision) {
        if (proc(rnd, dodge)) {
            return 0;
        } else {
            return max(ceil(damage - defense), ceil(damage * 0.2d)) * (proc(rnd, precision) ? 1.4d : 1d);
        }
    }

    private boolean proc(Random rnd, double value) {
        return rnd.nextInt(10000) < 100 * value;
    }

}
