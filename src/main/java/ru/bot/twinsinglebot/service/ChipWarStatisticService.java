package ru.bot.twinsinglebot.service;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.apache.commons.lang3.ArrayUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.telegram.telegrambots.meta.api.methods.pinnedmessages.PinChatMessage;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;
import ru.bot.twinsinglebot.component.RfInfoMessageParser;
import ru.bot.twinsinglebot.component.TelegramBot;
import ru.bot.twinsinglebot.config.TelegramBotProperties;
import ru.bot.twinsinglebot.data.document.Message;
import ru.bot.twinsinglebot.data.pojo.cw.ChipWarResult;
import ru.bot.twinsinglebot.data.pojo.cw.ChipWarUserResult;
import ru.bot.twinsinglebot.data.repository.mongo.ChipWarResultRepository;
import ru.bot.twinsinglebot.data.repository.mongo.MessageRepository;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.function.Predicate;

import static java.lang.String.format;
import static java.time.LocalDateTime.ofEpochSecond;
import static java.time.ZoneOffset.ofHours;
import static java.time.temporal.ChronoUnit.HOURS;
import static org.apache.commons.lang3.StringUtils.containsAny;

@Slf4j
@Service
@RequiredArgsConstructor
public class ChipWarStatisticService {

    private final ChipWarResultRepository chipWarResultRepository;

    private final MessageRepository messageRepository;

    private final RfInfoMessageParser rfInfoMessageParser;

    private final TelegramBot bot;

    private final TelegramBotProperties props;

    private final RestTemplate restTemplate;

    @Value("${pastebin.apikey}")
    private String apiKey;

    @Scheduled(cron = "0 * * * * *")
    private void computeStat() {
        log.debug("computeStat start");
        var last = chipWarResultRepository.findFirstByOrderByDateDesc();
        log.debug("computeStat last: {}", last);
        Message end;
        if (last == null) {
            end = messageRepository.findFirstByTextOrderByIdDesc("Война окончена!");
        } else {
            end = messageRepository.findFirstByTextAndDateAfterOrderByIdDesc("Война окончена!", last.getDate());
        }
        log.debug("computeStat end: {}", end);

        var start = messageRepository.findFirstByTextOrderByIdDesc("Война в краговых шахтах начнется через час!");
        log.debug("computeStat start: {}", start);
        if (last != null && start.getDate() < last.getDate()) {
            log.debug("computeStat end");
            return;
        }

        var now = (int) (System.currentTimeMillis() / 1000);
        var bound = now - 7200;
        if (end != null && end.getDate() > start.getDate() || start.getDate() < bound) {
            var messages = messageRepository.findByDateBetween(start.getDate(), now);
            var results = rfInfoMessageParser.computeResults(messages);
            if (!results.isEmpty()) {
                var result = new ChipWarResult(obtainStartTime(start.getDate()), results);
                chipWarResultRepository.save(result);
                sendStat(result);
            }
        }

        log.debug("computeStat end");
    }

    private int obtainStartTime(int seconds) {
        var zoneOffset = ZoneOffset.ofHours(3);
        var ldt = LocalDateTime.ofEpochSecond(seconds, 0, zoneOffset).truncatedTo(HOURS);
        var hour = ldt.getHour();
        if (hour >= 19) {
            return (int) ldt.withHour(20).toEpochSecond(zoneOffset);
        } else if (hour >= 14) {
            return (int) ldt.withHour(15).toEpochSecond(zoneOffset);
        } else if (hour >= 9) {
            return (int) ldt.withHour(10).toEpochSecond(zoneOffset);
        } else {
            throw new RuntimeException();
        }
    }

    @SneakyThrows
    private void sendStat(ChipWarResult result) {
        log.debug("sendStat start");
        if (result == null) {
            return;
        }
        var sendMessage = new SendMessage();
        sendMessage.setChatId(props.getChatId());
        sendMessage.setText(buildMessage(result, "[TwinSingle]", "[TwinS]"));
        sendMessage.disableNotification();
        var res = bot.execute(sendMessage);
        var pinChatMessage = new PinChatMessage(props.getChatId(), res.getMessageId());
        pinChatMessage.setDisableNotification(true);

        try {
            bot.execute(pinChatMessage);
        } catch (TelegramApiRequestException e) {
            if (!"Bad Request: not enough rights to pin a message".equals(e.getApiResponse())) {
                log.error(e.getMessage());
            }
        } finally {
            log.debug("sendStat end");
        }

        sendToMain(result);
    }

    private String buildMessage(ChipWarResult result, String... guilds) {
        var builder = new StringBuilder();
        for (var userResult : result.getResults()) {
            if (!containsAny(userResult.getNick(), guilds)) {
                continue;
            }
            builder.append(format("[%d/%d]%s\n",
                    userResult.getWins(),
                    userResult.getLoses(),
                    userResult.getNick()
            ));
        }
        return builder.toString();
    }

    private void sendToMain(ChipWarResult result) {
        log.debug("sendToMain start");
        var url = sendToPastebin(result);

        try {
            var sendMessage = new SendMessage();
            sendMessage.setChatId(props.getMainChatId());
            sendMessage.setText("Статистика по ЧВ:\n" + url);
            bot.execute(sendMessage);
        } catch (TelegramApiRequestException e) {
            log.error(e.toString(), e);
        } catch (TelegramApiException e) {
            log.error(e.getMessage(), e);
        } finally {
            log.debug("sendToMain end");
        }
    }

    private String sendToPastebin(ChipWarResult result) {
        var dtf = DateTimeFormatter.ofPattern("HH:mm dd/MM/YYYY");
        var name = "Chip War stats " + ofEpochSecond(result.getDate(), 0, ofHours(3)).format(dtf);

        var results = result.getResults();

        var basilaris = obtainResults(results, e -> e.getNick().startsWith("\uD83D\uDC69\u200D\uD83D\uDE80"));
        var castitas = obtainResults(results, e -> e.getNick().startsWith("\uD83E\uDDDD\u200D♀"));
        var aquilla = obtainResults(results, e -> e.getNick().startsWith("\uD83E\uDD16"));
        var total = obtainResults(results, e -> true);

        var args = new Integer[0];
        args = ArrayUtils.addAll(args, basilaris);
        args = ArrayUtils.addAll(args, castitas);
        args = ArrayUtils.addAll(args, aquilla);
        args = ArrayUtils.addAll(args, total);

        var body = format("" +
                        "Basilaris: %d белок, %d фрагов, %d смертей\n" +
                        "Castitas: %d эльфов, %d фрагов, %d смертей\n" +
                        "Aquilla: %d жестянок, %d фрагов, %d смертей\n" +
                        "Итого: %d участников, %d фрагов, %d смертей\n\n",
                (Object[]) args
        ) + buildMessage(result, "");

        var headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        var map = new LinkedMultiValueMap<String, String>();
        map.add("api_option", "paste");
        map.add("api_dev_key", apiKey);
        map.add("api_paste_name", name);
        map.add("api_paste_private", "0");
        map.add("api_paste_code", body);

        var url = restTemplate.postForObject("/api_post.php", new HttpEntity<>(map, headers), String.class);
        log.debug("sendToPastebin url: {}", url);
        return url;
    }

    private Integer[] obtainResults(List<ChipWarUserResult> results, Predicate<ChipWarUserResult> predicate) {
        var list = StreamEx.of(results).filter(predicate).toList();
        var wins = list.stream().map(ChipWarUserResult::getWins).mapToInt(Integer::intValue).sum();
        var loses = list.stream().map(ChipWarUserResult::getLoses).mapToInt(Integer::intValue).sum();
        return new Integer[]{list.size(), wins, loses};
    }

}
