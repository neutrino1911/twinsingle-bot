package ru.bot.twinsinglebot.service;

import com.google.common.html.HtmlEscapers;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.EditMessageText;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.meta.api.objects.replykeyboard.buttons.InlineKeyboardButton;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;
import ru.bot.twinsinglebot.component.TelegramBot;
import ru.bot.twinsinglebot.component.event.UpdateEvent;
import ru.bot.twinsinglebot.config.TelegramBotProperties;
import ru.bot.twinsinglebot.data.pojo.DungeonInvite;
import ru.bot.twinsinglebot.data.pojo.User;
import ru.bot.twinsinglebot.data.repository.mongo.UserRepository;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

import static java.lang.Integer.parseInt;
import static java.lang.String.format;
import static org.apache.commons.lang3.StringUtils.containsAny;
import static org.apache.commons.lang3.StringUtils.defaultString;

@Slf4j
@Service
@RequiredArgsConstructor
public class DungeonService {

    private final Map<String, DungeonInvite> invites = new ConcurrentHashMap<>();

    private final Map<String, Set<Integer>> receivers = new ConcurrentHashMap<>();

    private final Map<String, Long> queries = new ConcurrentHashMap<>();

    private final UserRepository userRepository;

    private final TelegramBot bot;

    private final TelegramBotProperties props;

    @Async
    @EventListener(UpdateEvent.class)
    public void updateHandler(UpdateEvent event) {
        var update = event.getUpdate();
        if (update.hasMessage()) {
            if (update.getMessage().getChat().isUserChat()) {
                handleMessage(update.getMessage());
            }
        } else if (update.hasCallbackQuery()) {
            var data = update.getCallbackQuery().getData();
            var key = data.substring(0, data.lastIndexOf(':'));
            if (queries.containsKey(key)) {
                return;
            }
            queries.put(key, System.currentTimeMillis());

            log.debug("updateHandler data: {}", data);
            var split = data.split(":");
            if ("DungeonLevels".equals(split[0])) {
                dungeonLevelsHandler(split);
            } else if ("DungeonGuild".equals(split[0])) {
                dungeonGuildHandler(split);
            }
        }
    }

    @SneakyThrows(TelegramApiException.class)
    private void handleMessage(Message message) {
        var forwardFrom = message.getForwardFrom();
        if (forwardFrom == null || forwardFrom.getId() != props.getBotId()) {
            return;
        }
        var text = message.getText();
        if (text == null || !text.startsWith("Ты создал группу")) {
            return;
        }
        var fromId = message.getFrom().getId();
        var byId = userRepository.findById(fromId);
        if (byId.isEmpty()) {
            var sendMessage = new SendMessage();
            sendMessage.setChatId(message.getChat().getId());
            sendMessage.setReplyToMessageId(message.getMessageId());
            sendMessage.setText("Профиль не найден. Перешли в лс профиль героя и попробуй еще раз.");
            bot.execute(sendMessage);
            return;
        }
        var user = byId.get();
        var cmd = text.substring(87);
        var invite = invites.computeIfAbsent(cmd, DungeonInvite::new);
        invite.setDate(message.getForwardDate());
        invite.setLeader(user);

        var sendMessage = new SendMessage();
        sendMessage.setChatId(message.getChat().getId());
        sendMessage.setReplyToMessageId(message.getMessageId());
        sendMessage.setText("Укажи уровень данжа");
        sendMessage.setReplyMarkup(getLevelsKeyboard(cmd));

        var res = bot.execute(sendMessage);

        invite.setMessageId(res.getMessageId());
    }

    private InlineKeyboardMarkup getLevelsKeyboard(String cmd) {
        var keyboardMarkup = new InlineKeyboardMarkup();
        var keyboard = List.of(
                List.of(new InlineKeyboardButton("1-5"), new InlineKeyboardButton("6-10")),
                List.of(new InlineKeyboardButton("11-15"), new InlineKeyboardButton("16-20")),
                List.of(new InlineKeyboardButton("21-25"), new InlineKeyboardButton("26-30")),
                List.of(new InlineKeyboardButton("31-35"), new InlineKeyboardButton("36-40")),
                List.of(new InlineKeyboardButton("41-45"), new InlineKeyboardButton("46-50"))
        );
        keyboard.get(0).get(0).setCallbackData(prepareLevelsKeyboardButtonData(cmd, "1"));
        keyboard.get(0).get(1).setCallbackData(prepareLevelsKeyboardButtonData(cmd, "6"));
        keyboard.get(1).get(0).setCallbackData(prepareLevelsKeyboardButtonData(cmd, "11"));
        keyboard.get(1).get(1).setCallbackData(prepareLevelsKeyboardButtonData(cmd, "16"));
        keyboard.get(2).get(0).setCallbackData(prepareLevelsKeyboardButtonData(cmd, "21"));
        keyboard.get(2).get(1).setCallbackData(prepareLevelsKeyboardButtonData(cmd, "26"));
        keyboard.get(3).get(0).setCallbackData(prepareLevelsKeyboardButtonData(cmd, "31"));
        keyboard.get(3).get(1).setCallbackData(prepareLevelsKeyboardButtonData(cmd, "36"));
        keyboard.get(4).get(0).setCallbackData(prepareLevelsKeyboardButtonData(cmd, "41"));
        keyboard.get(4).get(1).setCallbackData(prepareLevelsKeyboardButtonData(cmd, "46"));
        keyboardMarkup.setKeyboard(keyboard);

        return keyboardMarkup;
    }

    private String prepareLevelsKeyboardButtonData(String cmd, String val) {
        return format("DungeonLevels:%s:%s", cmd, val);
    }

    @SneakyThrows(TelegramApiException.class)
    private void dungeonLevelsHandler(String[] data) {
        var cmd = data[1];
        var lvl = parseInt(data[2]);
        var invite = invites.get(cmd);
        invite.setMin(lvl);
        invite.setMax(lvl + 7);

        var leader = invite.getLeader();

        if (leader.getGuild() == null) {
            sendInvites(invite);
        } else {
            var editMessageText = new EditMessageText();
            editMessageText.setChatId(leader.getId().longValue());
            editMessageText.setMessageId(invite.getMessageId());
            editMessageText.setText("Кому отправить приглашения?");
            editMessageText.setReplyMarkup(getGuildKeyboard(cmd, leader.getGuild(), leader.getRace()));

            bot.execute(editMessageText);
        }
    }

    private InlineKeyboardMarkup getGuildKeyboard(String cmd, String guild, String race) {
        var keyboardMarkup = new InlineKeyboardMarkup();
        var keyboard = List.of(
                List.of(new InlineKeyboardButton(guild), new InlineKeyboardButton(race))
        );
        keyboard.get(0).get(0).setCallbackData(prepareGuildKeyboardButtonData(cmd, "0"));
        keyboard.get(0).get(1).setCallbackData(prepareGuildKeyboardButtonData(cmd, "1"));
        keyboardMarkup.setKeyboard(keyboard);

        return keyboardMarkup;
    }

    private String prepareGuildKeyboardButtonData(String cmd, String val) {
        return format("DungeonGuild:%s:%s", cmd, val);
    }

    private void dungeonGuildHandler(String[] data) {
        var cmd = data[1];
        var guildOnly = "0".equals(data[2]);
        var invite = invites.get(cmd);
        invite.setGuildOnly(guildOnly);
        sendInvites(invite);
    }

    @SneakyThrows(TelegramApiException.class)
    private void sendInvites(DungeonInvite invite) {
        var cmd = invite.getCmd();
        var leader = invite.getLeader();
        var min = invite.getMin() - 1;
        var max = invite.getMax() + 1;
        var race = leader.getRace();
        var users = invite.isGuildOnly()
                ? userRepository.findAllByLevelBetweenAndGuild(min, max, leader.getGuild())
                : userRepository.findAllByLevelBetweenAndRace(min, max, race);

        if (invite.isGuildOnly()) {
            if ("[TwinSingle]".equals(leader.getGuild())) {
                users.addAll(userRepository.findAllByLevelBetweenAndGuild(min, max, "[TwinS]"));
            } else if ("[TwinS]".equals(leader.getGuild())) {
                users.addAll(userRepository.findAllByLevelBetweenAndGuild(min, max, "[TwinSingle]"));
            }
        }

        sendInvites(users, invite);

        invites.remove(cmd);
        for (var query : queries.keySet()) {
            if (query.contains(cmd)) {
                queries.remove(query);
            }
        }

        var editMessageText = new EditMessageText();
        editMessageText.setChatId(leader.getId().longValue());
        editMessageText.setMessageId(invite.getMessageId());
        editMessageText.setText("Приглашения были успешно разосланы!");

        bot.execute(editMessageText);
    }

    private void sendInvites(List<User> users, DungeonInvite invite) {
        var leader = invite.getLeader();
        var key = format("%s:%d:%d", invite.getCmd(), leader.getId(), invite.getDate());
        var receivers = this.receivers.computeIfAbsent(key, k -> new HashSet<>());

        users.removeIf(e -> e.getId().equals(leader.getId()));
        users.removeIf(User::isBlocked);
        users.removeIf(e -> receivers.contains(e.getId()));

        users.sort(Comparator.comparingInt(User::getLevel));

        users.stream().filter(e -> "neutrino1911".equals(e.getNick())).findFirst().ifPresent(user -> {
            users.remove(user);
            sendInvite(receivers, invite, user, 5000);
        });

        for (int i = 0; i < users.size(); i++) {
            var user = users.get(i);
            if (i % 30 == 29) {
                sendInvite(receivers, invite, user, 1000);
            } else {
                sendInvite(receivers, invite, user, 0);
            }
        }
    }

    @SneakyThrows
    private void sendInvite(Set<Integer> receivers, DungeonInvite invite, User user, int pause) {
        var cmd = invite.getCmd();
        try {
            var sendMessage = new SendMessage();
            sendMessage.setChatId(user.getId().longValue());
            sendMessage.setText(getInviteMessage(invite));
            sendMessage.setReplyMarkup(getInviteKeyboard(cmd));
            sendMessage.enableHtml(true);
            bot.execute(sendMessage);
            receivers.add(user.getId());
            Thread.sleep(pause);
        } catch (TelegramApiRequestException e) {
            var apiResponse = e.getApiResponse();
            if (containsAny(apiResponse,
                    "user is deactivated",
                    "bot was blocked by the user",
                    "chat not found",
                    "bot can't initiate conversation with a user"
            )) {
                user.setBlocked(true);
                userRepository.save(user);
            } else {
                log.error(e + " " + user.getNick());
            }
        } catch (Exception e) {
            log.error(e.getMessage(), e);
        }
    }

    private InlineKeyboardMarkup getInviteKeyboard(String cmd) {
        var keyboardMarkup = new InlineKeyboardMarkup();
        var keyboard = List.of(List.of(new InlineKeyboardButton("Присоединиться")));
        keyboard.get(0).get(0).setUrl("http://t.me/share/url?url=" + cmd);
        keyboardMarkup.setKeyboard(keyboard);
        return keyboardMarkup;
    }

    private String getInviteMessage(DungeonInvite invite) {
        var leader = invite.getLeader();
        return format("Игрок <a href=\"tg://user?id=%d\">%s %s</a> зовет тебя в подземелье %d-%d",
                leader.getId(),
                HtmlEscapers.htmlEscaper().escape(defaultString(leader.getGuild())),
                HtmlEscapers.htmlEscaper().escape(leader.getNick()),
                invite.getMin(),
                invite.getMin() + 4);
    }

    @Scheduled(cron = "0 0 * * * *")
    void clean() {
        var bound = System.currentTimeMillis() - 86_400_000L;
        for (var key : queries.keySet()) {
            if (queries.get(key) <= bound) {
                queries.remove(key);
            }
        }
        for (var key : receivers.keySet()) {
            var date = parseInt(key.substring(key.lastIndexOf(':') + 1));
            if (date <= bound) {
                receivers.remove(key);
            }
        }
    }

}
