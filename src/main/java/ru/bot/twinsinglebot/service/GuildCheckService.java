package ru.bot.twinsinglebot.service;

import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.methods.updatingmessages.DeleteMessage;
import org.telegram.telegrambots.meta.api.objects.Message;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.meta.exceptions.TelegramApiRequestException;
import ru.bot.twinsinglebot.component.TelegramBot;
import ru.bot.twinsinglebot.component.event.NewMessageEvent;
import ru.bot.twinsinglebot.config.TelegramBotProperties;
import ru.bot.twinsinglebot.data.repository.mongo.UserRepository;

@Slf4j
@Service
@RequiredArgsConstructor
public class GuildCheckService {

    protected final TelegramBot bot;

    protected final TelegramBotProperties props;

    protected final UserRepository userRepository;

    @Async
    @EventListener(NewMessageEvent.class)
    @SneakyThrows(TelegramApiException.class)
    public void updateHandler(NewMessageEvent event) {
        var message = event.getMessage();
        if (message.getForwardFrom() == null || message.getForwardFrom().getId() != props.getBotId()) {
            return;
        }
        var text = message.getText();
        if (text == null || !text.startsWith("Название:") || !text.contains("\uD83C\uDFE6 Банк:")) {
            return;
        }
        var found = false;
        var builder = new StringBuilder();
        for (var line : text.split("\\n")) {
            if (found) {
                var nick = line.substring(line.indexOf(')') + 2, line.lastIndexOf(' '));
                var byNick = userRepository.findByNick(nick);
                if (byNick.isPresent()) {
                    builder.append(line).append(" ✅\n");
                } else {
                    builder.append(line).append(" ❌\n");
                }
            } else if (line.startsWith("\uD83D\uDC65 Состав")) {
                found = true;
            }
        }

        tryRemoveMessage(message);

        var sendMessage = new SendMessage();
        sendMessage.setChatId(message.getChatId());
        sendMessage.setText(builder.toString());
        //editMessageText.enableMarkdown(true);

        try {
            bot.execute(sendMessage);
        } catch (TelegramApiRequestException e) {
            log.error(e.getMessage());
            log.error(e.getApiResponse());
        }
    }

    private void tryRemoveMessage(Message message) {
        var chatId = message.getChatId();
        var messageId = message.getMessageId();
        if (chatId == 0 || messageId == 0) {
            return;
        }
        var deleteMessage = new DeleteMessage(chatId, messageId);
        try {
            bot.execute(deleteMessage);
        } catch (TelegramApiException e) {
            log.error(e.getMessage());
        }
    }

}
