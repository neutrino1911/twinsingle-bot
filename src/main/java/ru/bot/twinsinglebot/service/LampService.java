package ru.bot.twinsinglebot.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import ru.bot.twinsinglebot.data.enums.Lamp;
import ru.bot.twinsinglebot.data.pojo.LampResult;
import ru.bot.twinsinglebot.data.pojo.User;
import ru.bot.twinsinglebot.data.pojo.battle.Unit;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class LampService {

    private final BattleService battleService;

    public List<LampResult> lamp(Unit user) {
        var baseResult = battleService.computeResult(user, user);
        var results = new ArrayList<LampResult>();
        for (var lamp : Lamp.values()) {
            var copy = copyUser(user);
            copy.setHealth(copy.getHealth() * (1 + lamp.health() / 100));
            copy.setDamage(copy.getDamage() * (1 + lamp.damage() / 100));
            copy.setDefense(copy.getDefense() * (1 + lamp.defense() / 100));
            var result = battleService.computeResult(copy, user);
            results.add(new LampResult(lamp, result.getWins() - baseResult.getWins()));
        }
        return results;
    }

    private User copyUser(Unit user) {
        var newUser = new User();
        newUser.setLevel(50);
        newUser.setHealth(user.getHealth());
        newUser.setDamage(user.getDamage());
        newUser.setDefense(user.getDefense());
        newUser.setDodge(user.getDodge());
        newUser.setPrecision(user.getPrecision());
        return newUser;
    }

}
