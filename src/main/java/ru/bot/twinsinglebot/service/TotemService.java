package ru.bot.twinsinglebot.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import one.util.streamex.StreamEx;
import org.springframework.stereotype.Service;
import ru.bot.twinsinglebot.data.enums.Totem;
import ru.bot.twinsinglebot.data.pojo.TotemResult;
import ru.bot.twinsinglebot.data.pojo.User;
import ru.bot.twinsinglebot.data.pojo.battle.BattleResult;
import ru.bot.twinsinglebot.data.pojo.battle.Unit;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

import static java.lang.String.format;
import static java.util.Comparator.comparing;
import static ru.bot.twinsinglebot.data.enums.Totem.ARES;
import static ru.bot.twinsinglebot.data.enums.Totem.CRONUS;
import static ru.bot.twinsinglebot.data.enums.Totem.HEPHAESTUS;
import static ru.bot.twinsinglebot.data.enums.Totem.POSEIDON;
import static ru.bot.twinsinglebot.data.enums.Totem.ZEUS;

@Slf4j
@Service
@RequiredArgsConstructor
public class TotemService {

    private final Map<String, List<TotemResult>> nextTotemsCache = new ConcurrentHashMap<>();

    private final Map<String, List<TotemResult>> totemsCache = new ConcurrentHashMap<>();

    private final BattleService battleService;

    public List<TotemResult> nextTotems(User user, int count) {
        var key = obtainKey(user, count);
        log.debug("nextTotems key: {}", key);
        var results = nextTotemsCache.get(key);
        if (results != null) {
            log.debug("nextTotems return from nextTotemsCache");
            return results;
        }

        var a = user.getAres();
        var p = user.getPoseidon();
        var h = user.getHephaestus();
        var z = user.getZeus();
        var c = user.getCronus();

        results = new ArrayList<>();

        outer:
        for (int i = 0; i < count; i++) {
            List<TotemResult> totemResults;
            Optional<TotemResult> bestOptional;

            var j = 0;
            do {
                totemResults = computeResults(user, user, a + j, p + j, h + j, z + j, c + j);
                if (totemResults.isEmpty()) {
                    break outer;
                }
                bestOptional = StreamEx
                        .of(totemResults)
                        .filter(e -> !e.getAdenPerWin().equals(BigDecimal.ZERO))
                        .min(comparing(TotemResult::getAdenPerWin));
                j++;
            } while (bestOptional.isEmpty());

            var best = bestOptional.get();

            if (j > 1) {
                best.setLevel(best.getLevel() + 1 - j);
                best.setCost(best.getTotem().costs()[best.getLevel()].intValue());
                best.setWins(0);
                best.setAdenPerWin(BigDecimal.ZERO);
            }

            log.debug("nextTotem best: {}", best);

            switch (best.getTotem()) {
                case ARES:
                    user.setDamage(calcStat(user.getDamage(), ARES, ++a));
                    break;
                case POSEIDON:
                    user.setDefense(calcStat(user.getDefense(), POSEIDON, ++p));
                    break;
                case HEPHAESTUS:
                    user.setDodge(user.getDodge() - HEPHAESTUS.stats()[h] + HEPHAESTUS.stats()[++h]);
                    break;
                case ZEUS:
                    user.setPrecision(user.getPrecision() - ZEUS.stats()[z] + ZEUS.stats()[++z]);
                    break;
                case CRONUS:
                    user.setHealth(calcStat(user.getHealth(), CRONUS, ++c));
                    break;
            }

            results.add(best);
        }

        nextTotemsCache.put(key, results);

        log.debug("nextTotems results: {}", results);
        return results;
    }

    public List<TotemResult> totems(User user) {
        return totems(user, user);
    }

    public List<TotemResult> totems(User user, Unit enemy) {
        var key = obtainKey(user, enemy instanceof User ? -1 : enemy.getLevel());
        log.debug("totems key: {}", key);
        var results = totemsCache.get(key);
        if (results != null) {
            log.debug("totems return from totemsCache");
            return results;
        }

        var a = user.getAres();
        var p = user.getPoseidon();
        var h = user.getHephaestus();
        var z = user.getZeus();
        var c = user.getCronus();

        results = computeResults(user, enemy, a, p, h, z, c);

        totemsCache.put(key, results);

        log.debug("totems results: {}", results);
        return results;
    }

    private String obtainKey(User user, int count) {
        return format("%.2f:%.2f:%.2f:%.2f:%.2f:%d:%d:%d:%d:%d:%d",
                user.getHealth(),
                user.getDamage(),
                user.getDefense(),
                user.getDodge(),
                user.getPrecision(),
                user.getAres(),
                user.getPoseidon(),
                user.getHephaestus(),
                user.getZeus(),
                user.getCronus(),
                count
        );
    }

    private List<TotemResult> computeResults(User hero, Unit enemy, int a, int p, int h, int z, int c) {
        var results = new ArrayList<TotemResult>();

        var baseResult = obtainMonsterResult(hero, enemy);

        if (++a < ARES.stats().length) {
            var newUser = copyUser(hero);
            newUser.setDamage(calcStat(hero.getDamage(), ARES, a));
            results.add(obtainTotemResult(ARES, a, baseResult, obtainMonsterResult(newUser, enemy)));
        }

        if (++p < POSEIDON.stats().length) {
            var newUser = copyUser(hero);
            newUser.setDefense(calcStat(hero.getDefense(), POSEIDON, p));
            results.add(obtainTotemResult(POSEIDON, p, baseResult, obtainMonsterResult(newUser, enemy)));
        }

        if (++h < HEPHAESTUS.stats().length) {
            var newUser = copyUser(hero);
            newUser.setDodge(hero.getDodge() - HEPHAESTUS.stats()[h - 1] + HEPHAESTUS.stats()[h]);
            results.add(obtainTotemResult(HEPHAESTUS, h, baseResult, obtainMonsterResult(newUser, enemy)));
        }

        if (++z < ZEUS.stats().length) {
            var newUser = copyUser(hero);
            newUser.setPrecision(hero.getPrecision() - ZEUS.stats()[z - 1] + ZEUS.stats()[z]);
            results.add(obtainTotemResult(ZEUS, z, baseResult, obtainMonsterResult(newUser, enemy)));
        }

        if (++c < CRONUS.stats().length) {
            var newUser = copyUser(hero);
            newUser.setHealth(calcStat(newUser.getHealth(), CRONUS, c));
            results.add(obtainTotemResult(CRONUS, c, baseResult, obtainMonsterResult(newUser, enemy)));

        }

        return results;
    }

    private double calcStat(double stat, Totem totem, int lvl) {
        var curMod = 1 + totem.stats()[lvl - 1] / 100;
        var newMod = 1 + totem.stats()[lvl] / 100;
        return stat / curMod * newMod;
    }

    private User copyUser(User user) {
        var newUser = new User();
        newUser.setLevel(50);
        newUser.setHealth(user.getHealth());
        newUser.setDamage(user.getDamage());
        newUser.setDefense(user.getDefense());
        newUser.setDodge(user.getDodge());
        newUser.setPrecision(user.getPrecision());
        return newUser;
    }

    private BattleResult obtainMonsterResult(User user, Unit enemy) {
        return battleService.computeResult(user, enemy);
    }

    private TotemResult obtainTotemResult(Totem totem, int level, BattleResult base, BattleResult current) {
        var deltaWins = current.getWins() - base.getWins();
        var apw = deltaWins <= 0
                ? BigDecimal.ZERO
                : totem.costs()[level].divide(BigDecimal.valueOf(deltaWins), 10, RoundingMode.HALF_UP);
        return new TotemResult(
                totem,
                level,
                totem.costs()[level].intValue(),
                deltaWins,
                apw
        );
    }

}
